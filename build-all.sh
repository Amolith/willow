#!/usr/bin/env sh

# SPDX-FileCopyrightText: Amolith <amolith@secluded.site>
#
# SPDX-License-Identifier: CC0-1.0

export CGO_ENABLED=0

if ! git describe --tags --exact-match HEAD; then
    echo "Not a tagged commit, refusing to build for all platforms."
    exit 0
fi

TAG=$(git describe --tags --exact-match HEAD)
NAME=$(basename "$(pwd)")

mkdir -p "out/$TAG"

while read -r LOOP_OS LOOP_ARCH; do
    echo "Building $NAME-$LOOP_OS-$LOOP_ARCH"
    GOOS="$LOOP_OS" GOARCH="$LOOP_ARCH" go build -ldflags="-s -w" -o "out/$TAG/$NAME-$LOOP_OS-$LOOP_ARCH" ./cmd
done <<EOF
darwin amd64
freebsd 386
freebsd amd64
freebsd arm
freebsd arm64
linux 386
linux amd64
linux arm
linux arm64
linux ppc64le
linux riscv64
netbsd amd64
openbsd amd64
openbsd arm64
windows amd64
EOF
