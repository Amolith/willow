<!--
SPDX-FileCopyrightText: Amolith <amolith@secluded.site>

SPDX-License-Identifier: CC0-1.0
-->

# Contributing to Willow

_Let us know if anything's missing from here and we'll expand it!_

Contributions are very much welcome! Please take a look at the [ticket
tracker][todo] and see if there's anything you're interested in working on. If
there's specific functionality you'd like to see implemented and it's not
mentioned in the ticket tracker, please describe it through one of [the
communication platforms](#communication) below so we can discuss its inclusion.
If we don't feel like it fits with Willow's goals, you're encouraged to fork the
project and make whatever changes you like!

### Collaboration

Some people dislike GitHub, some people dislike SourceHut, and some people
dislike both. Collaboration happens on multiple platforms so anyone can
contribute to Willow however they like. Any of the following are suitable, but
they're listed in order of Amolith's preference:

- [SourceHut]
  - **Distributed:** contributions are either through [git send-email], which
    requires you to have SMTP access to an email address, or through SourceHut's
    web UI, which requires a SourceHut account.
  - **Open source:** SourceHut components are licenced under AGPL, BSD, and
    possibly others.
  - <details><summary>Configuring git for <code>git send-email</code> (click to expand)</summary>
    <a href="https://git-send-email.io">git-send-email.io</a> is a great resource for getting started. Please run the following commands before submitting patches to Willow.
    <pre>
    git config sendemail.to "~amolith/willow@lists.sr.ht"
    git config format.subjectPrefix "PATCH willow"
    </pre>
    </details>
- [Radicle]
  - **Distributed:** contributions are through the [Heartwood protocol], which
    requires you to at least set up a local Radicle node.
  - **Open source:** Radicle components are licenced under Apache, MIT, GPL, and
    possibly others.
- [Codeberg]
  - **Centralised:** contributions are through Codeberg pull requests and
    require a Codeberg account.
  - **Open source:** Codeberg is powered by Forgejo, which is licensed under
    GPLv3.
- [GitHub]
  - **Centralised:** contributions are through GitHub pull requests and require
    a GitHub account.
  - **Mixed:** _components_ of GitHub are open source, such as the syntax
    highlighter, but everything that makes GitHub _useful_ is proprietary.

[SourceHut]: https://sr.ht/~amolith/willow
[git send-email]: https://git-send-email.io
[Radicle]: https://radicle.secluded.site/nodes/seed.secluded.site/rad:z34saeE8jnN5KbGRuLSggJ3eeLtew
[Heartwood protocol]: https://radicle.xyz/guides/protocol
[Codeberg]: https://codeberg.org/Amolith/willow
[GitHub]: https://github.com/Amolith/willow

### Communication

Questions, comments, and patches can always go to the [mailing list][email], but
there's also an [IRC channel][irc] and an [XMPP MUC][xmpp] for real-time
interactions.

- Email: [~amolith/willow@lists.sr.ht][email]
- IRC: [irc.libera.chat/#willow][irc]
- XMPP: [willow@muc.secluded.site][xmpp]

[email]: mailto:~amolith/willow@lists.sr.ht
[irc]: ircs://irc.libera.chat/#willow
[xmpp]: xmpp:willow@muc.secluded.site?join
[todo]: https://todo.sr.ht/~amolith/willow

_If you haven't used mailing lists before, please take a look at [SourceHut's
documentation](https://man.sr.ht/lists.sr.ht/), especially the etiquette
section._

### Before committing anything...

Please make sure you configure git to sign [the DCO] in each commit. We won't
accept contributions without DCO signatures.

``` shell
git config format.signOff yes
```

[the DCO]: https://developercertificate.org/

### Required tools

- [Go](https://go.dev/)
- [gofumpt](https://github.com/mvdan/gofumpt)
  - Stricter formatting rules than the default `go fmt`
- [golangci-lint](https://golangci-lint.run/)
  - Aggregates various preinstalled Go linters, runs them in parallel, and makes
    heavy use of the Go build cache
- [Staticcheck](https://staticcheck.dev/)
  - Uses static analysis to find bugs and performance issues, offer
    simplifications, and enforce style rules

### Suggested tools

- [just](https://github.com/casey/just)
  - Command runner to simplify use of the required tools
- [air](https://github.com/cosmtrek/air)
  - Watches source files and rebuilds/executes the project when sources change

#### Using Air

Running `air` with no arguments from inside Willow's repo launches Air using our
supplied config. Willow runs on [localhost:1313](http://localhost:1313) directly
and Air proxies Willow at [localhost:8888](http://localhost:8888) to provide
live reloading. Air notices when you save source files, rebuilds and re-launches
the binary automatically, and reloads the page in your browser for you.
