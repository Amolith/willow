<!--
SPDX-FileCopyrightText: Amolith <amolith@secluded.site>

SPDX-License-Identifier: CC0-1.0
-->

# Willow

[![Go report card status][goreportcard-badge]][goreportcard]
[![REUSE status][reuse-shield]][reuse]
[![Donate with fosspay][fosspay-shield]][fosspay]

[goreportcard-badge]: https://goreportcard.com/badge/git.sr.ht/~amolith/willow
[goreportcard]: https://goreportcard.com/report/git.sr.ht/~amolith/willow
[reuse]: https://api.reuse.software/info/git.sr.ht/~amolith/willow
[reuse-shield]: https://shields.io/reuse/compliance/git.sr.ht/~amolith/willow
[fosspay]: https://secluded.site/donate/
[fosspay-shield]: https://shields.io/badge/donate-fosspay-yellow

_Forge-agnostic software release tracker_

![screenshot of willow's current web UI](.files/2024-02-24.png)

_This UI is Amolith's attempt at a balance between simple, pleasant, and
functional. Amolith is not a UX professional and would **very** much welcome
input from someone more knowledgeable!_

## What is it?

_If you'd rather watch a short video, Amolith gave a 5-minute [lightning talk on
Willow] at the 2023 Ubuntu Summit._

[lightning talk on Willow]: https://youtu.be/XIGxKyekvBQ?t=29900

Willow helps developers, sysadmins, and homelabbers keep up with software
releases across arbitrary forge platforms, including full-featured forges like
GitHub, GitLab, or [Forgejo] as well as more minimal options like [cgit] or
[stagit].

[Forgejo]: https://forgejo.org/
[cgit]: https://git.zx2c4.com/cgit/
[stagit]: https://codemadness.org/stagit.html

It exists because decentralisation, as wonderful as it is, does have some pain
points. One piece of software is on GitHub, another piece is on GitLab, one on
Bitbucket, a fourth on [SourceHut], a fifth on the developer's self-hosted
Forgejo instance.

[SourceHut]: https://sourcehut.org/

The capabilities of each platform can also differ, further complicating the
space. For example, Forgejo and GitHub have APIs and RSS release feeds,
SourceHut has an API and RSS feeds that notify you of _all_ activity in the
repo, GitLab only has an API, and there's no standard for discovering the
capabilities of arbitrary git frontends like [legit].

[legit]: https://github.com/icyphox/legit

And _then_ you have different pieces of information in different places; some
developers might publish release announcements on their personal blog and some
projects might release security advisories on an external platform prior to
publishing a release.

All this important info is scattered all over the internet. Willow brings some
order to that chaos by supporting both RSS and one of the _very_ few things all
the forges and frontends have in common: their **V**ersion **C**ontrol
**S**ystem. At the moment, [Git] is the _only_ supported VCS, but we're
definitely interested in adding support for [Pijul], [Fossil], [Mercurial], and
potentially others.

[Git]: https://git-scm.com/
[Pijul]: https://pijul.org/
[Fossil]: https://www.fossil-scm.org/
[Mercurial]: https://www.mercurial-scm.org/

Amolith (the creator) has recorded some of his other ideas, thoughts, and plans
in [his wiki].

[his wiki]: https://wiki.secluded.site/hypha/willow

## Installation and use

**Disclaimers:**
- Docker image coming soon™
- We consider the project _alpha-quality_. There will be bugs.
- Amolith has tried to make the web UI accessible, but is unsure of its current
  usability.
- The app is not localised yet and English is the only available language.
- Help with any/all of the above is most welcome!

### Installation

This assumes Willow will run on an always-on server, like a VPS.

- Obtain the binary appropriate for your system from one of the release pages
  (they're all the same)
  - [SourceHut](https://git.sr.ht/~amolith/willow/refs)
  - [Codeberg](https://codeberg.org/Amolith/willow/releases)
  - [GitHub](https://github.com/Amolith/willow/releases)
- Make sure you're in the same folder as the binary when running the following
  commands
- Mark the binary as executable with `chmod +x willow`
- Execute the binary with `./willow`
- Edit the config with `nano config.toml`
- Daemonise Willow using systemd, OpenRC, etc.
- Reverse-proxy the web UI (defaults to `localhost:1313`) with Caddy, NGINX,
  etc.

### Use

- Create a user with `./willow -a <username>`
- Open the web UI (defaults to `localhost:1313`, but [installation] had you put
  a proxy in front)
- Click `Track new project`
- Fill out the form and press `Next`
- Indicate which version you're currently on and press `Track releases`
- You're now tracking that project's releases!

[installation]: #installation

If you no longer use that project, click the `Delete?` link to remove it, and,
if applicable, Willow's copy of its repo.

If you're no longer running the version Willow says you've selected, click the
`Modify?` link to select a different version.

If there are projects where your selected version does _not_ match what Willow
thinks is latest, they'll show up at the top under the **Outdated projects**
heading and have a link at the bottom of the card to `View release notes`.
Clicking that link populates the right column with those release notes.

If there are projects where your selected version _does_ match what Willow
thinks is latest, they'll show up at the bottom under the **Up-to-date
projects** heading.

### Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
